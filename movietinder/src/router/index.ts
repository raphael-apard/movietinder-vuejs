import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Home from "../views/Home.vue";
import Inscription from "../views/Inscription.vue";
import Connexion from "../views/Connexion.vue";
import ChangerMdp from "../views/ChangerMdp.vue";
import Recherche from "../views/Recherche.vue";
import { BootstrapVue, BootstrapVueIcons } from "bootstrap-vue";

Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  // {
  //   path: "/mes-recommandations",
  //   name: "MesRecommandations",
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () =>
  //     import(/* webpackChunkName: "about" */ "../views/MesRecommandations.vue"),
  // },
  {
    path: "/connexion",
    name: "Connexion",
    component: Connexion,
  },
  {
    path: "/inscription",
    name: "Inscription",
    component: Inscription,
  },
  {
    path: "/changer-mdp",
    name: "ChangerMdp",
    component: ChangerMdp,
  },
  {
    path: "/recherche",
    name: "Recherche",
    component: Recherche,
  },
  {
    path: "/recommandations",
    name: "Recommandation",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Recommandation.vue"),
  },
  {
    path: "/mes-recommandations",
    name: "MesRecommandations",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/MesRecommandations.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
